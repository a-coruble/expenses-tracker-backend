import pytest
import requests
from unittest.mock import patch

from main.apps.expenses.utils import get_exchange_rate, convert_to_currency


@patch("requests.get")
def test_get_exchange_rate_valid_response(mock_requests_get):
    mock_requests_get.return_value.status_code = 200
    mock_requests_get.return_value.json.return_value = {
        "rate": 1
    }
    response = get_exchange_rate("EUR", "EUR")
    assert mock_requests_get.called
    assert response == 1


@patch("requests.get")
def test_get_exchange_rate_invalid_response(mock_requests_get):
    mock_requests_get.return_value.status_code = 400
    response = get_exchange_rate("EUR", "EUR")
    assert mock_requests_get.called
    assert response == None


@patch("main.apps.expenses.utils.get_exchange_rate")
def test_convert_to_currency_invalid_rate(mock_get_exchange_rate):
    mock_get_exchange_rate.return_value = None
    response = convert_to_currency(100, "EUR", "USD")
    assert mock_get_exchange_rate.called
    assert response == None
