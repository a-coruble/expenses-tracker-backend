import pytest
from decimal import Decimal
from unittest.mock import patch
from collections import OrderedDict

from main.apps.expenses.models import Bank, Transaction
from main.apps.expenses.constants import EURO, UNITED_STATES_DOLLAR, DEBIT, CREDIT

pytestmark = pytest.mark.django_db


def test_bank_viewset_list(api_client, simple_user):
    api_client.force_authenticate(user=simple_user)

    url = "/api/V0/banks/"
    response = api_client.get(url, format="json")

    assert response.status_code == 200
    assert len(response.data) == 0

    us_bank = Bank.objects.create(user=simple_user, currency=UNITED_STATES_DOLLAR, amount=100000.0, name="Chase")
    eu_bank = Bank.objects.create(user=simple_user, currency=EURO, amount=500000.0, name="LCL")

    response = api_client.get(url, format="json")
    assert response.status_code == 200
    assert len(response.data) == 2
    assert response.data == [
        OrderedDict(
            [
                ("id", eu_bank.id),
                ("currency", eu_bank.currency),
                ("amount", "500000.0000"),
                ("name", eu_bank.name)
            ]
        ), OrderedDict(
            [
                ("id", us_bank.id),
                ("currency", us_bank.currency),
                ("amount", "100000.0000"),
                ("name", us_bank.name)
            ]
        )
    ]


def test_bank_viewet_create(api_client, simple_user, bank_json):
    api_client.force_authenticate(user=simple_user)

    url = "/api/V0/banks/"
    response = api_client.post(url, bank_json, format="json")
    assert response.status_code == 201
    assert len(response.data) == 4
    assert response.data == {"id": 3, "currency": "USD", "amount": "100000.0000", "name": "Chase"}


def test_bank_viewet_update(api_client, simple_user, bank_json):
    bank = Bank.objects.create(user=simple_user, currency=EURO, amount=500000.0, name="LCL")
    assert Bank.objects.count() == 1

    api_client.force_authenticate(user=simple_user)
    url = "/api/V0/banks/{}/".format(bank.id)
    response = api_client.patch(url, bank_json)
    assert response.status_code == 200
    assert len(response.data) == 4
    assert response.data == {'id': bank.id, 'currency': 'USD', 'amount': '100000.0000', 'name': 'Chase'}


def test_bank_viewset_delete(api_client, simple_user):
    bank = Bank.objects.create(user=simple_user, currency=EURO, amount=500000.0, name="LCL")
    assert Bank.objects.count() == 1

    api_client.force_authenticate(user=simple_user)
    url = "/api/V0/banks/{}/".format(bank.id)
    response = api_client.delete(url)
    assert response.status_code == 204
    assert Bank.objects.count() == 0


def test_transaction_viewset_list(api_client, simple_user):
    api_client.force_authenticate(user=simple_user)

    url = "/api/V0/transactions/"
    response = api_client.get(url, format="json")
    assert response.status_code == 200
    assert len(response.data) == 0

    bank1 = Bank.objects.create(user=simple_user, currency=EURO, amount=500000.0, name="LCL")
    bank2 = Bank.objects.create(user=simple_user, currency=UNITED_STATES_DOLLAR, amount=500000.0, name="Chase")
    assert Bank.objects.count() == 2

    transaction_1 = Transaction.objects.create(bank=bank1, amount=100000.0, currency=EURO, transaction_type=CREDIT)
    transaction_2 = Transaction.objects.create(bank=bank2, amount=100000.0, currency=EURO, transaction_type=DEBIT)

    response = api_client.get(url, format="json")
    assert response.status_code == 200
    assert len(response.data) == 2
    assert response.data == [
        OrderedDict(
            [
                ('id', transaction_2.id),
                ('bank', transaction_2.bank.id),
                ('amount', '100000.0000'),
                ('currency', transaction_2.currency),
                ('message', transaction_2.message),
                ('transaction_type', transaction_2.transaction_type)
            ]
        ), OrderedDict(
            [
                ('id', transaction_1.id),
                ('bank', transaction_1.bank.id),
                ('amount', '100000.0000'),
                ('currency', transaction_1.currency),
                ('message', transaction_1.message),
                ('transaction_type', transaction_1.transaction_type)
            ]
        )
    ]

    url = url + "?bank={}".format(bank1.id)

    response = api_client.get(url, format="json")
    assert response.status_code == 200
    assert len(response.data) == 1
    assert response.data == [
        OrderedDict(
            [
                ('id', transaction_1.id),
                ('bank', transaction_1.bank.id),
                ('amount', '100000.0000'),
                ('currency', transaction_1.currency),
                ('message', transaction_1.message),
                ('transaction_type', transaction_1.transaction_type)
            ]
        )
    ]


def test_transaction_viewset_delete(simple_user, api_client):
    api_client.force_authenticate(user=simple_user)

    bank = Bank.objects.create(user=simple_user, currency=EURO, amount=500000.0, name="LCL")
    assert Bank.objects.count() == 1

    transaction = Transaction.objects.create(bank=bank, amount=100000.0, currency=EURO, transaction_type=CREDIT)
    assert Transaction.objects.count() == 1

    url = "/api/V0/transactions/{}/".format(transaction.id)
    response = api_client.delete(url)
    assert response.status_code == 204
    assert Transaction.objects.count() == 0


@patch("main.apps.expenses.utils.get_exchange_rate", autospec=True)
def test_transaction_viewset_create_same_currency(mock_get_exchange_rate, simple_user, api_client):
    mock_get_exchange_rate.return_value = 1

    bank = Bank.objects.create(user=simple_user, currency=EURO, amount=500000.0, name="LCL")
    assert Bank.objects.count() == 1

    api_client.force_authenticate(user=simple_user)

    url = "/api/V0/transactions/add_transaction/"
    data = {
        "bank": bank.id,
        "amount": 100000.0,
        "currency": EURO,
        "transaction_type": CREDIT
    }
    response = api_client.post(url, data, format="json")
    assert mock_get_exchange_rate.called
    assert response.status_code == 200
    assert response.data == Decimal(bank.amount + data["amount"])


@patch("main.apps.expenses.utils.get_exchange_rate", autospec=True)
def test_transaction_viewset_create_different_currency(mock_get_exchange_rate, simple_user, api_client):
    mock_get_exchange_rate.return_value = 1

    bank = Bank.objects.create(user=simple_user, currency=EURO, amount=500000.0, name="LCL")
    assert Bank.objects.count() == 1

    api_client.force_authenticate(user=simple_user)

    url = "/api/V0/transactions/add_transaction/"
    data = {
        "bank": bank.id,
        "amount": 100000.0,
        "currency": UNITED_STATES_DOLLAR,
        "transaction_type": CREDIT
    }
    response = api_client.post(url, data, format="json")
    assert mock_get_exchange_rate.called
    assert response.status_code == 200
    assert response.data == Decimal(bank.amount + data["amount"])
