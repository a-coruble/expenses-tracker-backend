import pytest
import json


@pytest.fixture()
def bank_json():
    with open("main/apps/expenses/tests/jsons/new_bank.json") as f:
        data = json.load(f)
    return data
