from django.urls import path, include
from rest_framework.routers import DefaultRouter

from main.apps.expenses.viewsets import BankViewset, TransactionViewset

router = DefaultRouter()

router.register("banks", BankViewset, base_name="banks")
router.register("transactions", TransactionViewset, base_name="transactions")

urlpatterns = [
    path("V0/", include(router.urls)),
]
