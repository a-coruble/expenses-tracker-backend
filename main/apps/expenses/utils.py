import requests
import decimal

from main.apps.expenses.constants import EXCHANGE_RATE_URL


def get_exchange_rate(fromCurrency, toCurrency):
    url = EXCHANGE_RATE_URL.format(
        fromCurrency=fromCurrency, toCurrency=toCurrency)
    response = requests.get(url)
    if response.status_code != 200:
        return None
    return response.json()["rate"]


def convert_to_currency(amount, fromCurrency, toCurrency):
    rate = get_exchange_rate(fromCurrency, toCurrency)
    if not rate:
        return None
    return amount * decimal.Decimal(rate)
