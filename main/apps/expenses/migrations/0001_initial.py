# Generated by Django 2.1 on 2018-09-11 22:28

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Bank',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('currency', models.CharField(choices=[('EUR', 'Euro'), ('USD', 'United States Dollar'), ('CAD', 'Canadian Dollar'), ('AUD', 'Australian Dollar')], max_length=3)),
                ('amount', models.DecimalField(decimal_places=4, max_digits=16)),
                ('name', models.CharField(max_length=256)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'get_latest_by': 'modified',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('amount', models.DecimalField(decimal_places=4, max_digits=16)),
                ('currency', models.CharField(choices=[('EUR', 'Euro'), ('USD', 'United States Dollar'), ('CAD', 'Canadian Dollar'), ('AUD', 'Australian Dollar')], max_length=3)),
                ('message', models.CharField(blank=True, max_length=256, null=True)),
                ('transaction_type', models.IntegerField(choices=[(-1, 'Debit'), (1, 'Credit')], default=-1)),
                ('bank', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='expenses.Bank')),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'get_latest_by': 'modified',
                'abstract': False,
            },
        ),
    ]
