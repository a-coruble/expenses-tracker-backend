from django.contrib import admin

from main.apps.expenses.models import Bank, Transaction
# Register your models here.

admin.site.register((Bank, Transaction))
