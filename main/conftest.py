import pytest

from django.contrib.auth.models import User
from rest_framework.test import APIClient


@pytest.fixture()
def api_client():
    return APIClient()


@pytest.fixture()
def simple_user():
    u = User.objects.create_user("user", "user@gmail.com", "test")
    return u
